data "template_file" "cloudtrail_kms" {
  template = "${file("${path.module}/templates/iam_kms_policy_for_cloudtrail.json.tpl")}"

  vars {
    aws_account_id = "${var.aws_account_id}"
  }
}

resource "aws_kms_key" "cloudtrail" {
  description             = "Encrypt/Decrypt cloudtrail logs"
  deletion_window_in_days = 7
  is_enabled              = true
  enable_key_rotation     = true
  policy                  = "${data.template_file.cloudtrail_kms.rendered}"

  tags {
    product = "${var.tag_product}"
    env     = "${var.tag_env}"
    role    = "audit"
  }
}

resource "aws_kms_alias" "cloudtrail" {
  name          = "alias/${var.tag_product}-${var.tag_env}-cloudtrail"
  target_key_id = "${aws_kms_key.cloudtrail.key_id}"
}

resource "aws_cloudtrail" "cloudtrail" {
  name                          = "${var.tag_product}-${var.tag_env}"
  s3_bucket_name                = "${aws_s3_bucket.audit.id}"
  is_multi_region_trail         = true
  include_global_service_events = true
  enable_log_file_validation    = true
  kms_key_id                    = "${aws_kms_key.cloudtrail.arn}"

  event_selector {
    read_write_type           = "All"
    include_management_events = true

    data_resource {
      type = "AWS::S3::Object"

      values = [
        "arn:aws:s3",
      ]
    }
  }
}

data "template_file" "s3_bucket_policy" {
  template = "${file("${path.module}/templates/s3_bucket_policy_audit.json.tpl")}"

  vars {
    bucket_name = "${var.tag_product}-${var.tag_env}-audit"
  }
}

resource "aws_s3_bucket" "audit" {
  bucket        = "${var.tag_product}-${var.tag_env}-audit"
  force_destroy = false
  policy        = "${data.template_file.s3_bucket_policy.rendered}"

  tags {
    product = "${var.tag_product}"
    env     = "${var.tag_env}"
    role    = "audit"
  }
}

# every lambda function uses this assume role policy
data "template_file" "iam-lambda-assume-role-policy" {
  template = "${file("${path.module}/templates/iam-lambda-assume-role-policy.json.tpl")}"
}

# AWS Access Key check and delete function
### IAM policies
data "template_file" "key_age_check_policy" {
  template = "${file("${path.module}/templates/key_age_check_policy.json.tpl")}"
}

resource "aws_iam_role" "key_age_check" {
  name               = "${var.tag_product}-${var.tag_env}-key-age-check-lambda"
  assume_role_policy = "${data.template_file.iam-lambda-assume-role-policy.rendered}"
}

resource "aws_iam_role_policy" "key_age_check" {
  name   = "${var.tag_product}-${var.tag_env}-key-age-check-lambda"
  role   = "${aws_iam_role.key_age_check.id}"
  policy = "${data.template_file.key_age_check_policy.rendered}"
}

##/IAM policies
## ZIP the file
data "archive_file" "key_age_check" {
  type        = "zip"
  source_file = "${path.module}/templates/key_age_check.py"
  output_path = "${path.module}/files/key_age_check.zip"
}

##/ ZIP the file
## Create the lambda function
resource "aws_lambda_function" "key_age_check" {
  filename         = "${path.module}/files/key_age_check.zip"
  function_name    = "${var.tag_product}-${var.tag_env}-key-age-check"
  role             = "${aws_iam_role.key_age_check.arn}"
  handler          = "key_age_check.lambda_handler"
  source_code_hash = "${data.archive_file.key_age_check.output_base64sha256}"
  runtime          = "python2.7"
  timeout          = 180

  environment {
    variables = {
      DRY_RUN         = "${var.dry_run}"
      EMAIL_TO_DOMAIN = "${var.email_to_domain}"
      EMAIL_FROM      = "${var.email_from}"
      KEY_AGE_NOTIFY  = "${var.key_age_notify}"
      KEY_AGE_MAX     = "${var.key_age_max}"
      SES_REGION      = "${var.ses_region}"
    }
  }

  tags {
    product = "${var.tag_product}"
    env     = "${var.tag_env}"
    role    = "audit-helper"
  }
}

##/ Create the lambda function
## Schedule the lambda function
resource "aws_cloudwatch_event_rule" "key_age_check" {
  name                = "${var.tag_product}-${var.tag_env}-key-age-check"
  description         = "deletes access keys that older than 60 days"
  schedule_expression = "cron(0 6 * * ? *)"
}

resource "aws_cloudwatch_event_target" "key_age_check" {
  rule      = "${aws_cloudwatch_event_rule.key_age_check.name}"
  target_id = "${var.tag_product}-${var.tag_env}-key-age-check"
  arn       = "${aws_lambda_function.key_age_check.arn}"
}

resource "aws_lambda_permission" "key_age_check" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.key_age_check.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.key_age_check.arn}"
}

##/ Schedule the lambda function
#/ AWS Access Key check and delete function

# MFA check and disable function
data "template_file" "mfa_check_policy" {
  template = "${file("${path.module}/templates/mfa_check_policy.json.tpl")}"
}

resource "aws_iam_role" "mfa_check" {
  name               = "${var.tag_product}-${var.tag_env}-mfa-check-lambda"
  assume_role_policy = "${data.template_file.iam-lambda-assume-role-policy.rendered}"
}

resource "aws_iam_role_policy" "mfa_check" {
  name   = "${var.tag_product}-${var.tag_env}-mfa-check-lambda"
  role   = "${aws_iam_role.mfa_check.id}"
  policy = "${data.template_file.mfa_check_policy.rendered}"
}

##/IAM policies
## ZIP the file
data "archive_file" "mfa_check" {
  type        = "zip"
  source_file = "${path.module}/templates/mfa_check.py"
  output_path = "${path.module}/files/mfa_check.zip"
}

##/ ZIP the file
## Create the lambda function
resource "aws_lambda_function" "mfa_check" {
  filename         = "${path.module}/files/mfa_check.zip"
  function_name    = "${var.tag_product}-${var.tag_env}-mfa-check"
  role             = "${aws_iam_role.mfa_check.arn}"
  handler          = "mfa_check.lambda_handler"
  source_code_hash = "${data.archive_file.mfa_check.output_base64sha256}"
  runtime          = "python2.7"
  timeout          = 180

  environment {
    variables = {
      DRY_RUN         = "${var.dry_run}"
      EMAIL_TO_DOMAIN = "${var.email_to_domain}"
      EMAIL_FROM      = "${var.email_from}"
      SES_REGION      = "${var.ses_region}"
    }
  }

  tags {
    product = "${var.tag_product}"
    env     = "${var.tag_env}"
    role    = "audit-helper"
  }
}

##/ Create the lambda function
## Schedule the lambda function
resource "aws_cloudwatch_event_rule" "mfa_check" {
  name                = "${var.tag_product}-${var.tag_env}-mfa-check"
  description         = "disables users without MFA"
  schedule_expression = "cron(0 6 * * ? *)"
}

resource "aws_cloudwatch_event_target" "mfa_check" {
  rule      = "${aws_cloudwatch_event_rule.mfa_check.name}"
  target_id = "${var.tag_product}-${var.tag_env}-mfa-check"
  arn       = "${aws_lambda_function.mfa_check.arn}"
}

resource "aws_lambda_permission" "mfa_check" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.mfa_check.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.mfa_check.arn}"
}

##/ Schedule the lambda function
#/ MFA check and disable function

# Inactive user check and disable function
data "template_file" "inactivity_check_policy" {
  template = "${file("${path.module}/templates/disable_inactive_users_policy.json.tpl")}"
}

resource "aws_iam_role" "inactivity_check" {
  name               = "${var.tag_product}-${var.tag_env}-inactivity-check-lambda"
  assume_role_policy = "${data.template_file.iam-lambda-assume-role-policy.rendered}"
}

resource "aws_iam_role_policy" "inactivity_check" {
  name   = "${var.tag_product}-${var.tag_env}-inactivity-check-lambda"
  role   = "${aws_iam_role.inactivity_check.id}"
  policy = "${data.template_file.inactivity_check_policy.rendered}"
}

##/IAM policies
## ZIP the file
data "archive_file" "inactivity_check" {
  type        = "zip"
  source_file = "${path.module}/templates/disable_inactive_users.py"
  output_path = "${path.module}/files/disable_inactive_users.zip"
}

##/ ZIP the file
## Create the lambda function
resource "aws_lambda_function" "inactivity_check" {
  filename         = "${path.module}/files/disable_inactive_users.zip"
  function_name    = "${var.tag_product}-${var.tag_env}-inactivity-check"
  role             = "${aws_iam_role.inactivity_check.arn}"
  handler          = "disable_inactive_users.lambda_handler"
  source_code_hash = "${data.archive_file.inactivity_check.output_base64sha256}"
  runtime          = "python2.7"
  timeout          = 180

  environment {
    variables = {
      DRY_RUN          = "${var.dry_run}"
      EMAIL_TO_DOMAIN  = "${var.email_to_domain}"
      EMAIL_FROM       = "${var.email_from}"
      INACTIVITY_LIMIT = "${var.user_inactivity_limit}"
      SES_REGION       = "${var.ses_region}"
    }
  }

  tags {
    product = "${var.tag_product}"
    env     = "${var.tag_env}"
    role    = "audit-helper"
  }
}

##/ Create the lambda function
## Schedule the lambda function
resource "aws_cloudwatch_event_rule" "inactivity_check" {
  name                = "${var.tag_product}-${var.tag_env}-inactivity-check"
  description         = "disables users without login inactivity more than ${var.user_inactivity_limit}"
  schedule_expression = "cron(0 6 * * ? *)"
}

resource "aws_cloudwatch_event_target" "inactivity_check" {
  rule      = "${aws_cloudwatch_event_rule.inactivity_check.name}"
  target_id = "${var.tag_product}-${var.tag_env}-inactivity-check"
  arn       = "${aws_lambda_function.inactivity_check.arn}"
}

resource "aws_lambda_permission" "inactivity_check" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.inactivity_check.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.inactivity_check.arn}"
}

##/ Schedule the lambda function
#/ Inactive user check and disable function

