{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:DeleteLoginProfile",
        "iam:GetAccessKeyLastUsed",
        "iam:GetUser",
        "iam:ListUsers",
        "iam:ListAccessKeys",
        "ses:SendEmail"
      ],
      "Resource": "*"
    }
  ]
}
