import os
import boto3
import datetime

def send_notifications(expired_keys):
    client = boto3.client('ses', region_name=os.environ['SES_REGION'])
    for (user, keys) in expired_keys.items():
        destination = {
            'ToAddresses': ['%s@%s' % (user, os.environ['EMAIL_TO_DOMAIN'])]
        }

        message = {
            'Subject': {
                'Data': '[IMPORTANT] AWS IAM Access Key Rotation'
            },
            'Body': {
                'Html': {
                    'Data': """Following access keys will be expired soon.
                            Go to AWS Console and replace them with fresh ones.
                            If you do not change your keys, all will be deleted.
                            %s
                            """ % '\n'.join(keys)
                }
            }
        }

        if 'DRY_RUN' in os.environ and (os.environ['DRY_RUN'] == "0" or os.environ['DRY_RUN'] == "false"):
            client.send_email(
                Source = os.environ['EMAIL_FROM'],
                Destination=destination,
                Message=message,
            )
        else:
            print destination, message
            print "DRY_RUN is set. The notification email is not sent"

def mask_key(key):
    masked =""
    if type(key) is str:
        masked = key[:5] + '*' * (len(key) - 10) + key[-5:]
    elif type(key) is list:
        for i in key:
            masked = masked + mask_key(i)
    return masked

def lambda_handler(event, context):
    iam = boto3.client('iam')
    users = iam.list_users(MaxItems=123)

    key_age_max = int(os.environ['KEY_AGE_MAX'])
    key_age_notify = int(os.environ['KEY_AGE_NOTIFY'])
    print('Notify: ' + str(key_age_notify) + ', Expire: ' + str(key_age_max))

    expired_keys = {}

    for user in users['Users']:
        if not user['UserName'].startswith('svc') and not user['UserName'].startswith('service'):
            print('Processing ' + user['UserName'])
            access_keys = iam.list_access_keys(UserName=user['UserName'])
            for key in access_keys['AccessKeyMetadata']:
                today = datetime.datetime.utcnow().replace(tzinfo=key['CreateDate'].tzinfo)
                delta = today - key['CreateDate']
                if delta.days < key_age_max and delta.days >= key_age_max - key_age_notify:
                    print '@' + str(delta.days) + ':' + key['UserName'] + '/' + mask_key(key['AccessKeyId']) + ' will be expired in ' + str(key_age_max - delta.days)
                    expired_keys.setdefault(key["UserName"], []).append(mask_key(key['AccessKeyId']))
                elif delta.days >= key_age_max:
                    print '@' + str(delta.days) + ':' + key['UserName'] + '/' + mask_key(key['AccessKeyId']) + ' is expired. The key created ' + str(delta.days) + ' days ago. Will be deleted'
                    if 'DRY_RUN' in os.environ and (os.environ['DRY_RUN'] == "0" or os.environ['DRY_RUN'] == "false"):
                        iam.delete_access_key(UserName=key['UserName'], AccessKeyId=key['AccessKeyId'])
                    else:
                        print "DRY_RUN has been set and the key is not deleted"
                else:
                    print '@' + str(delta.days) + ':' + key['UserName'] + ' ' + mask_key(key['AccessKeyId'])+ ' is OK. ' + str(delta.days)
    send_notifications(expired_keys)

# if __name__ == "__main__":
#    event = 1
#    context = 1
#    lambda_handler(event, context)
