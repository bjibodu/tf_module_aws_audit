import os
import boto3
import datetime

def send_notifications(users):
    client = boto3.client('ses', region_name=os.environ['SES_REGION'])
    for user in users:
        destination = {
            'ToAddresses': ['%s@%s' % (user, os.environ['EMAIL_TO_DOMAIN'])]
        }

        message = {
            'Subject': {
                'Data': '[IMPORTANT] AWS MFA Monitoring'
            },
            'Body': {
                'Html': {
                    'Data': """Your account has been disabled due to no MFA setup
                            Please ask any of Cloud Platform engineer to enable it
                            Do not forget to set up MFA at first login!.
                            """
                }
            }
        }

        if 'DRY_RUN' in os.environ and (os.environ['DRY_RUN'] == "0" or os.environ['DRY_RUN'] == "false"):
            client.send_email(
                Source = os.environ['EMAIL_FROM'],
                Destination=destination,
                Message=message,
            )
        else:
            print destination, message
            print "DRY_RUN is set. The notification email is not sent"

def lambda_handler(event, context):
    iam = boto3.client('iam')
    users = iam.list_users()
    disable_users = []

    for user in users['Users']:
        if not user['UserName'].startswith('svc') and not user['UserName'].startswith('service'):
            print('Processing ' + user['UserName'])
            mfa = iam.list_mfa_devices(UserName=user['UserName'])
            if not mfa['MFADevices']:
                disable_users.append(user['UserName'])
                if 'DRY_RUN' in os.environ and (os.environ['DRY_RUN'] == "0" or os.environ['DRY_RUN'] == "false"):
                    iam.delete_login_profile(UserName=user['UserName'])
                else:
                    print "DRY_RUN has been set and the %s is not disabled" % user['UserName']
    send_notifications(disable_users)

# if __name__ == "__main__":
#    event = 1
#    context = 1
#    lambda_handler(event, context)
