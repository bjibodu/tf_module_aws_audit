import os
import boto3
import datetime

iam = boto3.client('iam')
inactivity_limit = int(os.environ['INACTIVITY_LIMIT'])

def send_notifications(inactive_users):
    client = boto3.client('ses', region_name=os.environ['SES_REGION'])
    for user in inactive_users:
        destination = {
            'ToAddresses': ['%s@%s' % (user, os.environ['EMAIL_TO_DOMAIN'])]
        }

        message = {
            'Subject': {
                'Data': '[IMPORTANT] AWS IAM Inactivity Check'
            },
            'Body': {
                'Html': {
                    'Data': """Your account has been disabled due to %s days inactivity.
                            Please ask for help to Cloud Platform Team
                            """ % os.environ['INACTIVITY_LIMIT']
                }
            }
        }

        if 'DRY_RUN' in os.environ and (os.environ['DRY_RUN'] == "0" or os.environ['DRY_RUN'] == "false"):
            client.send_email(
                Source = os.environ['EMAIL_FROM'],
                Destination=destination,
                Message=message,
            )
        else:
            print destination, message
            print "DRY_RUN is set. The notification email is not sent"

def is_user_logged_in(user_name):
    user_details = iam.get_user(UserName=user_name)
    if 'PasswordLastUsed' in user_details['User']:
        last_login = user_details['User']['PasswordLastUsed']
    else:
        last_login = user_details['User']['CreateDate']

    today = datetime.datetime.utcnow().replace(tzinfo=last_login.tzinfo)
    last_login_delta = today - last_login

    if last_login_delta.days > inactivity_limit:
        return False

    return True

def is_access_key_used(access_key_id):
    access_key_last_used = iam.get_access_key_last_used(AccessKeyId=access_key_id)
    # if the key is never used, there is no LastUsedDate item
    if 'LastUsedDate' in access_key_last_used['AccessKeyLastUsed']:
        last_used = access_key_last_used['AccessKeyLastUsed']['LastUsedDate']
        today = datetime.datetime.utcnow().replace(tzinfo=last_used.tzinfo)
        access_key_last_used_delta = today - last_used
        if access_key_last_used_delta.days > inactivity_limit:
            return False
    return True

def lambda_handler(event, context):
    print('Inactivity Limit ' + str(inactivity_limit))
    inactive_users = []

    users = iam.list_users()

    for user in users['Users']:
        if not user['UserName'].startswith('svc') and not user['UserName'].startswith('service'):
            print('Processing ' + user['UserName'])
            any_key_used = False

            logged_in = is_user_logged_in(user['UserName'])

            access_keys = iam.list_access_keys(UserName=user['UserName'])
            for key in access_keys['AccessKeyMetadata']:
                if is_access_key_used(key['AccessKeyId']):
                    any_key_used = True

            if not logged_in and not any_key_used:
                inactive_users.append(user['UserName'])
                if 'DRY_RUN' in os.environ and (os.environ['DRY_RUN'] == "0" or os.environ['DRY_RUN'] == "false"):
                    iam.delete_login_profile(UserName=user['UserName'])
                else:
                    print "DRY_RUN has been set and the %s is not disabled" % user['UserName']

    send_notifications(inactive_users)

# if __name__ == "__main__":
#    event = 1
#    context = 1
#    lambda_handler(event, context)
