variable "aws_region" {
  description = "Region for creating an S3 bucket for cloudtrail and config"
}

variable "aws_account_id" {
  description = "AWS Account ID for creating policies"
}

variable "dry_run" {
  description = "dry-runs lambda functions"
  default     = false
}

variable "email_to_domain" {
  description = "DNS of the org that we can complete user@[DNSNAME]"
}

variable "email_from" {
  description = "Sender name for outgoing emails"
}

variable "key_age_notify" {
  description = "Start to notify users N days before the key has been deleted"
}

variable "key_age_max" {
  description = "Delete key older than N days"
}

variable "user_inactivity_limit" {
  description = "Users that inactive more than limit will be disabled"
}

variable "tag_product" {}
variable "tag_env" {}

variable "ses_region" {
  description = "What region will be used for sending emails"
  default     = "eu-west-2"
}
